<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


$back_to_top_class = mfn_opts_get('back-top-top');

if( $back_to_top_class == 'hide' ){
	$back_to_top_position = false;
} elseif( strpos( $back_to_top_class, 'sticky' ) !== false ){
	$back_to_top_position = 'body';
} elseif( mfn_opts_get('footer-hide') == 1 ){
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}

?>

<?php do_action( 'mfn_hook_content_after' ); ?>


</div><!-- #Wrapper -->

<!-- #Footer -->		
<footer id="Footer" class="clearfix">
	
	<?php if ( $footer_call_to_action = mfn_opts_get('footer-call-to-action') ): ?>
	<div class="footer_action">
		<div class="container">
			<div class="column one column_column">
				<?php echo do_shortcode( $footer_call_to_action ); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="widgets_wrapper" style="">
		<div class="container">
			<div class="column one-fourth JLD-logo">
				<aside id="text-5" class="widget widget_text">			
					<div class="textwidget">
						<img src="https://salony-jeanlouisdavid.pl/wp-content/uploads/2016/12/logo2.png" alt="" width="409" height="76" class="footerLogo">
					</div>
				</aside>
			</div>
		<div class="column one-fourth JLD-join">
			<aside id="text-4" class="widget widget_text">			
				<div class="textwidget">
					<a target="_blank" class=" buttonFooter" data-tracking-label="Dołącz do zespołu" href="https://www.jeanlouisdavid.pl/dolacz-do-zespolu" title="Dołącz do zespołu">
						<p>Dołącz do zespołu</p>
					</a>
				</div>
			</aside>
		</div>
		<div class="column one-fourth JLD-newsletter">
			<aside id="text-3" class="widget widget_text">
				<div class="textwidget">
					<a class="buttonFooter"  href="#" data-popup-open="popup-1" data-tracking-action="Clic newsletter" data-tracking-label="NEWSLETTER" target="_blank" href="https://www.pages03.net/provalliancepolska/newsletter/zapisz-sie" title="Newsletter">NEWSLETTER</a>
				</div>
			</aside>
		</div>
		<div class="column one-fourth JLD-socials">
			<aside id="text-2" class="widget widget_text">
				<div class="textwidget">
					<ul class="social">
						<li class="facebook">
							<a target="_blank" href="https://www.facebook.com/jean.louis.david.polska" title="Facebook">
								<i class="icon-facebook"></i>
							</a>
						</li>
						<li class="instagram">
							<a target="_blank" href="https://www.instagram.com/jeanlouisdavidfr/" title="Instagram">
								<i class="icon-instagram"></i>
							</a>
						</li>
						<li class="twitter">
							<a target="_blank" href="https://twitter.com/JLDPolska" title="Twitter">
								<i class="icon-twitter"></i>
							</a>
						</li><br />
						<li class="pinterest">
							<a target="_blank" href="https://pl.pinterest.com/jeanlouisdavid/" title="Pinterest">
								<i class="icon-pinterest"></i>
							</a>
						</li>
						<li class="youtube">
							<a target="_blank" href="https://www.youtube.com/user/JeanLouisDavidPL" title="YouTube">
								<i class="icon-play"></i>
							</a>
						</li>
						<li class="googleplus">
							<a target="_blank" href="https://plus.google.com/+jeanlouisdavid" title="Google+">
								<i class="icon-gplus"></i>
							</a>
						</li>
					</ul>
				</div>
			</aside>
		</div>
		</div>
	</div>


	<?php if( mfn_opts_get('footer-hide') != 1 ): ?>
	
		<div class="footer_copy">
			<div class="container">
				<div class="column one">

					<?php 
						if( $back_to_top_position == 'copyright' ){
							echo '<a id="back_to_top" class="button button_left button_js" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>';
						}
					?>
					
					<!-- Copyrights -->
					<div class="copyright">
						<?php wp_nav_menu( array( 'theme_location' => 'footer-links' ) ); ?>
					</div>
	
				</div>
				<div class="column one">
					<div style="margin: 0px 1%;" class="column one_second">© 2016 Jean Louis Daivd. Wszelkie prawa zastrzeżone. </div> <div class="column one_second" style="float: right; margin: 0px 1%;"> Realizacja: <a href="http://network-interactive.pl/" target="_blank" title="Network Interactive" style="color: #f15a23;"><img src="/wp-content/uploads/2016/12/logo-network-interactive.png" alt="logo-network-interactive" width="30" height="30" class="footer-logo-network"></a> | <a class="link-wp" href="http://wordpress-wdrozenia.pl/" target="_blank" title="Wdrożenia Wordpress" style="color: #f15a23;">Wdrożenia WordPress</a></div>					</div>
				</div>
			</div>
		</div>
	
	<?php endif; ?>
	
	
	<?php 
		if( $back_to_top_position == 'footer' ){
			echo '<a id="back_to_top" class="button button_left button_js in_footer" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>';
		}
	?>

	
</footer>
<?php 
	// Responsive | Side Slide
	if( mfn_opts_get( 'responsive-mobile-menu' ) ){
		get_template_part( 'includes/header', 'side-slide' );
	}
?>

<?php
	if( $back_to_top_position == 'body' ){
		echo '<a id="back_to_top" class="button button_left button_js '. $back_to_top_class .'" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>';
	}
?>

<?php if( mfn_opts_get('popup-contact-form') ): ?>
	<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php mfn_opts_show( 'popup-contact-form-icon', 'icon-mail-line' ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php echo do_shortcode( mfn_opts_get('popup-contact-form') ); ?>
			<span class="arrow"></span>
		</div>
	</div>
<?php endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>
	
<!-- wp_footer() -->
<?php wp_footer(); ?>

<div class="popup" data-popup="popup-1">
    <div class="popup-inner">
		<h3 style="text-align:center;">Zapisz się <br />na nasz newsletter!</h3>
		<form id="newsletter_salony" method="post" action="http://www.pages03.net/provalliancepolska/salony-kosmetyczne/zapisz-sie" pageId="8073081" siteId="297379" parentPageId="8073079" >

            <div id="container_EMAIL">
               <div>
					<div class="fieldLabel" style="">Email<span class="required">*</span></div>
					<input type="text" name="Email" id="control_EMAIL" label="Email" class="textInput defaultText validate[required]" required="required" style="">
				</div>

            <div id="container_COLUMN10">
               <div>
                  <div class="fieldLabel" style="">Imię<span class="required">*</span></div>
               </div>
               <input type="text" name="First Name" id="control_COLUMN10" label="Imię" class="textInput defaultText validate[required]" required="required" style="">
            </div>

            <div id="container_COLUMN25">
               <div>
                  <div class="fieldLabel" style="">Kod pocztowy<span class="required">*</span></div>
               </div>
               <input type="text" name="Postal Code" id="control_COLUMN25" label="Kod pocztowy" class="textInput defaultText validate[required]" required="required" style="">
            </div>

            <div class="box_buttonStyle">
				<input type="submit" class="defaultText buttonStyle" value="ZAPISZ SIĘ">
            </div>

		<input type="hidden" name="Brand" id="control_COLUMN4" value="Jean Louis David">

		<input type="hidden" name="Company" id="control_COLUMN7" value="SalonyKosmetyczne Newsletter">

		<input type="hidden" name="Has Customer Panel" id="control_COLUMN38" value="No">

		<input type="hidden" name="Last Name" id="control_COLUMN14" value="SalonyKosmetyczne">
		<input type="hidden" name="formSourceName" value="StandardForm"><!-- DO NOT REMOVE HIDDEN FIELD sp_exp --><input type="hidden" name="sp_exp" value="yes">
		</div>
	</form>
        <a class="popup-close" data-popup-close="popup-1" href="#">x</a>
    </div>
	<div style="text-align:center; color:#fff;">Pomyślnie zapisałeś się na naszą listę mailingową.</div>
</div>


<script type='text/javascript' src='https://salony-jeanlouisdavid.pl/wp-content/themes/betheme/js/jquery.validationEngine.js'></script>
<script>
var days = 'sunday,monday,tuesday,wednesday,thursday,friday,saturday'.split(',');

document.getElementById( days[(new Date()).getDay()] ).className = 'fat-day';
</script>
</body>
</html>